package com.epam.java_basic;

import com.epam.java_basic.calculator.Calculator;
import com.epam.java_basic.utils.UserInterface;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application {

    public static void main(String[] args) {
        if (args.length == 0) {
            UserInterface.print("You should specify calculator precision through command line arguments!");
            System.exit(1);
        }
        new Application().start(Integer.valueOf(args[0]));
    }

    private void start(int precision) {
        Calculator calculator = new Calculator(precision);
        do {
            startCalculationIteration(calculator);
        } while (UserInterface.askYesNo("Do you want to continue? (Y/N)"));
        UserInterface.print("Bye!");
    }

    private void startCalculationIteration(Calculator calculator) {
        double a = UserInterface.askDouble("Enter the first number:");
        double b = UserInterface.askDouble("Enter the second number:");
        String operation = UserInterface.askWord("Enter operator (+, -, *, /):");
        double result = 0;
        boolean isPrintResult = true;
        switch (operation) {
        case "+":
            result = calculator.add(a, b);
            break;
        case "-":
            result = calculator.subtract(a, b);
            break;
        case "*":
            result = calculator.multiply(a, b);
            break;
        case "/":
            result = calculator.div(a, b);
            break;
        default:
            UserInterface.print("You choose wrong operation!");
            isPrintResult = false;
        }
        if (isPrintResult) {
            UserInterface.print(String.format("Result: %f", result));
        }
    }

}
